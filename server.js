const express = require('express');
const keys = require('./config/keys.js');

const app = express();

const mongoose = require('mongoose');
mongoose.connect(keys.mongoURI, { useNewUrlParser: true, useUnifiedTopology: true});

// setup database models
require('./model/Account');

// setup the routes
require('./routes/authenticationRoutes')(app);

app.listen(keys.port, () => {
    console.log("Listening on " + keys.port);
});
